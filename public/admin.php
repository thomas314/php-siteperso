<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ADMIN</title>
</head>
<body>
	<h1>Espace admin</h1>

	<?php
$json = file_get_contents(__DIR__.'/../data/last_message.json');
$msg = json_decode($json);
	?>

<dl>
	<dt>Nom</dt>
	<dd><?= $msg->name ?></dd>
	<dt>Message</dt>
	<dd><?= $msg->message ?></dd>
</dl>
</body>
</html>