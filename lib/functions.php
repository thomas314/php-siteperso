<?php

function getContent(){
	switch($_GET['pages'])
	{
		case 'bio':
			include __DIR__.'/../pages/bio.php';
			break;

		case 'contact':
			include __DIR__.'/../pages/contact.php';
			break;

		default:
			include __DIR__.'/../pages/home.php';
	}
	// if(!isset($_GET['pages'])){
	// 	include __DIR__.'/../pages/home.php';
	// } else {
	// 	$p = $_GET['p'];
	// 	$pages = ['bio', 'contact', 'home'];
	// 	$path = __DIR__ . '/../pages/' . $p . '.php';
	// 	if(in_array($p,$pages) && file_exists($path)) {
	// 		include $path;			
	// 	} else {
	// 		echo ('error 404');
	// 	}
	// }
}
function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}
function getUserData(){
	$json = file_get_contents(__DIR__. '/../data/user.json');
	$user = json_decode($json, true);
	return $user;
}


// Fonction trouvée sur stack overflow à modif /!\
// function Request($request) {
// 	function getBody()
//   {
//     if($this->requestMethod === "GET")
//     {
//       return;
//     }
//     if ($this->requestMethod == "POST")
//     {
//       $body = array();
//       foreach($_POST as $key => $value)
//       {
//         $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
//       }
//       return $body;
//     };
//   };
// };


?>